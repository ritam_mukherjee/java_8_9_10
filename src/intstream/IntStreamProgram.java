package intstream;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class IntStreamProgram {
    public static void main(String[] args) {
        int[] arr={1,0,0,4,5};
        int[] output=new int[arr.length];

        System.out.println("reverse print");
        IntStream.range(0,arr.length)
                .mapToObj(index -> String.format("%d -> %s", index, arr[index]))
                .forEach(System.out::println);

        int start=0;
        int end=arr.length;
        IntStream.range(start,end).map(i -> start +(end -1-i)).forEach(index -> {
            System.out.println("index:"+index+",value:"+arr[index]);
        });



        IntStream.range(0,arr.length).map(operand -> arr[operand]).forEach(value -> System.out.println(value));


        System.out.println("-----------------------------");
        IntStream.range(0,arr.length).map(operand -> arr[operand]);
        Arrays.stream(output).forEach(System.out::println);

        AtomicInteger outer_idex=new AtomicInteger();

        IntStream.range(0,arr.length).forEach(value -> {
            if(outer_idex.get()<output.length) {
                if (arr[value] == 0) {
                    output[outer_idex.getAndIncrement()] = arr[value];
                }
                output[outer_idex.getAndIncrement()] = arr[value];
            }
        });

        System.out.println(Arrays.toString(output));
    }
}
