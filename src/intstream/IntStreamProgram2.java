package intstream;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class IntStreamProgram2 {
    public static void main(String[] args) {
        int[] nums1 = {1,3,3};

        Arrays.stream(nums1).forEach(i -> System.out.println(i));


        /*Optional<Boolean> first = Arrays.stream(nums1).mapToObj(i -> nums1[i] == 0).findFirst();*/

int var=0;
        AtomicInteger ai=new AtomicInteger(
                (IntStream.range(0,nums1.length)
                    .filter(i -> nums1[i]==0).count()>0)
                        ?IntStream.range(0,nums1.length)
                        .filter(i -> nums1[i]==0).findFirst().getAsInt()
                :-1);
/*
        (IntStream.range(0,nums1.length)
                .filter(i -> nums1[i]==0).count()>0)?
        OptionalInt indexOpt = IntStream.range(0,nums1.length)
                .filter(i -> nums1[i]==0)
                .findAny();
*/

       // System.out.println(indexOpt.getAsInt());

    }
}
