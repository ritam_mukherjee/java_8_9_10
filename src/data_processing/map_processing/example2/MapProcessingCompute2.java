package data_processing.map_processing.example2;

import java.util.*;

public class MapProcessingCompute2 {
    public static void main(String[] args) {
        Employee employee1 = new Employee(11, "Ram");
        Employee employee2 = new Employee(22, "Shyam");
        Employee employee3 = new Employee(33, "Jadu");
        Employee employee4 = new Employee(44, "Madhu");
        Employee employee5 = new Employee(33, "Gopal");
        Employee employee6 = new Employee(44, "Rakhal");

        City kolkata = new City("Kolkata");
        City mumbai = new City("Mumbai");
        City delhi = new City("Delhi");
        City chennai = new City("Chennai");

        Map<City, List<Employee>> cityRegister = new HashMap<>();
        cityRegister.putIfAbsent(kolkata, Arrays.asList(employee1,employee2));
        cityRegister.putIfAbsent(mumbai, Arrays.asList(employee3,employee4));

        System.out.println(cityRegister);

        cityRegister.computeIfAbsent(delhi,city -> new ArrayList<>()).addAll(Arrays.asList(employee5,employee6));
        System.out.println(cityRegister);

    }
}
