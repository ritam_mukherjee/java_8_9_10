package data_processing.map_processing.example2;

import java.util.*;


public class MapProcessing2 {

    public static void main(String[] args) {
        Person p1 = new Person("Alice", 23);
        Person p2 = new Person("Brian", 56);
        Person p3 = new Person("Chelsea", 46);
        Person p4 = new Person("David", 28);
        Person p5 = new Person("Erica", 37);
        Person p6 = new Person("Francisco", 18);

        City kolkata = new City("Kolkata");
        City mumbai = new City("Mumbai");
        City delhi = new City("Delhi");
        City chennai = new City("Chennai");

        Map<City, List<Person>> cityRegister1 = new HashMap<>();
        cityRegister1.computeIfAbsent(kolkata, city -> new ArrayList<>()).add(p2);
        cityRegister1.computeIfAbsent(delhi, city -> new ArrayList<>()).add(p3);

        Map<City, List<Employee>> cityRegister2 = new HashMap<>();
        cityRegister1.computeIfAbsent(chennai, city -> new ArrayList<>()).add(p5);
        cityRegister1.computeIfAbsent(delhi, city -> new ArrayList<>()).add(p6);

        cityRegister1.replaceAll((city, people) -> {
            if(city.getName().equals("Kolkata"))
                people.stream().forEach(person -> person.setName("Mr."+person.getName()));
            return people;
        });

        System.out.println(cityRegister1);


    }

}
