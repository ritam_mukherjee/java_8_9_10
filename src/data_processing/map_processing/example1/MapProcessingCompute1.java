package data_processing.map_processing.example1;

import java.util.HashMap;
import java.util.Map;

public class MapProcessingCompute1 {
    public static void main(String[] args) {
      /*
        A new Family of method is compute * which takes mainly two parameters
            1. key
            2. a bi-function which takes a key-value pair a new value computeed out of this key
            this bi-function is called as remapping function.

            the key may or may not ve in the map
       */
        Map<String,Integer> map=new HashMap();

        map.put("AAA",111);
        map.put("BBB",222);
        map.put("CCC",333);
        map.put("DDD",444);
        map.put("EEE",555);
        map.put("FFF",null);

        map.compute("EEE",(s, integer) -> integer*10+integer%10);
      //  map.compute("FFF",(s, integer) -> integer*10+integer%10);
        System.out.println(map);


        /*the key 'FFF' is not present hence this block of code will create NullPinterException*/
       /*map.compute("FFF",(s, integer) -> integer*10+integer%10);*/


        /*This piece of code wil not create any problem*/
        map.computeIfPresent("FFF",(s, integer) -> integer*10+integer%10);
        System.out.println(map);
    }
}
