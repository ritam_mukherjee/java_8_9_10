package data_processing.map_processing.example1;

import java.util.HashMap;
import java.util.Map;

public class MapProcessingMerge1 {
    public static void main(String[] args) {
        Map<String,Integer> map1=new HashMap();
        map1.put("AAA",111);
        map1.put("BBB",222);
        map1.put("CCC",333);
        map1.put("FFF",666);  /*conflicting element*/
        map1.put("GGG",777);  /*conflicting element*/
        map1.put("HHH",999);


        Map<String,Integer> map2=new HashMap();
        map2.put("DDD",444);
        map2.put("EEE",555);
        map2.put("FFF",111); /*conflicting element*/
        map2.put("GGG",222); /*conflicting element*/
        map2.put("HHH",999);


        System.out.println(ThreadColor.ANSI_CYAN+"Two maps are there: ");
        System.out.println("map1 before Merge :"+map1);
        System.out.println("map2 before Merge :"+map2);
        System.out.println("----------------------------------------------");
        /*we are processing all elements of map2 and merging at map1*/
        map2.forEach(
                (key, value) ->
                        map1.merge(key,value,(map1_element, map2_element) -> map1_element+map2_element)
        );

        /*Here no elements are conflicting in both map, so just inserting */
        System.out.println(ThreadColor.ANSI_GREEN+"merging where no conflicting elements in map");
        System.out.println("map1 after Merge    :"+map1);
        System.out.println("map2 after Merge    :"+map2);



    }
}
