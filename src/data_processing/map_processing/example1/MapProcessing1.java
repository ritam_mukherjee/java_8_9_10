package data_processing.map_processing.example1;

import java.util.HashMap;
import java.util.Map;

public class MapProcessing1 {
    public static void main(String[] args) {

        Map<String,Integer> map=new HashMap();
        map.put("EEE",555);
        map.put("AAA",111);
        map.put("DDD",444);
        map.put("BBB",222);
        map.put("CCC",333);


       /* Performs the given action for each entry in this map until all entries have been processed
        or the action throws an exception.*/
        System.out.println(ThreadColor.ANSI_GREEN+"---------------forEach()---------------------------------");
        map.forEach((key, value) -> System.out.println("key:"+key +","+"Value:"+value));


        System.out.println(ThreadColor.ANSI_RED+"---------------getOrDefault()---------------------------------");
        /*if the key not present, 'get' return null where as getOrDefault return default value*/
        System.out.println("Map does not have 'FFF', hence get method return 'null' :-"+map.get("FFF"));
        System.out.println("the getOrDefault() method return default value :- "
                +map.getOrDefault("FFF",000));


        /*
        If the specified key is not already associated with a value (or is mapped to `null`) associates it
        with the given value and returns `null`, else returns the current value.
         */
        System.out.println(ThreadColor.ANSI_BLUE+"---------------putIfAbsent()---------------------------------");
        map.putIfAbsent("EEE",999);
        System.out.println("The value iof 'EEE' will not change as it is already exist in Map"+map);
        map.putIfAbsent("FFF",666);
        System.out.println("The value iof 'FFF' will insert as it is not existing in Map"+map);


        System.out.println(ThreadColor.ANSI_CYAN+"---------------replace()---------------------------------");
        /*
        Two version of replace method available
            1. replace(): if the key match replace the value
            2. if the key match and existing value match then only replace.
         */
        map.replace("CCC",444);
        System.out.println("The replace will happen whatever it value is :"+map);
        map.replace("BBB",333,777);
        System.out.println("The replace will not happen as existing value not matching :"+map);
        map.replace("BBB",222,777);
        System.out.println("The replace will  happen as existing value not matching :"+map);


        System.out.println(ThreadColor.ANSI_PURPLE+"---------------replaceAll()---------------------------------");
        /*
        Replaces each entry's value with the result of invoking the given Bi-function on that entry, until
        All entries have been processed or the function throws an exception.
        */
        map.replaceAll((key, value) -> {
            if(key.equals("BBB")||key.equals("DDD"))
                    return value-5;
                  else return value;
        });
        System.out.println("wherever condition match there only replacement happen"+map);
        map.replaceAll((key, value) -> ((Integer)value)%100);
        System.out.println("without any condition check value will replace"+map);
        System.out.println(map);

    }
}
