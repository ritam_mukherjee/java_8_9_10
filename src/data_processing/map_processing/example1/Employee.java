package data_processing.map_processing.example1;

public class Employee {
    private int empId;
    private String ename;

    public Employee(int empId, String ename) {
        this.empId = empId;
        this.ename = ename;
    }

    @Override
    public String toString() {
        return "{" +
                "empId=" + empId +
                ", ename='" + ename + '\'' +
                '}';
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }
}
