package map_reduce;

import java.util.Arrays;
import java.util.List;

import static map_reduce.ColorConstants.ANSI_CYAN;
import static map_reduce.ColorConstants.ANSI_GREEN;
import static map_reduce.ColorConstants.ANSI_RED;


public class ListProcessing {
    public static void main(String[] args) {
        List<Integer> naturalNumbers=Arrays.asList(1,2,3,4,5,6,7,8,9,0);
        List<Integer> evenNumbers=Arrays.asList(2,4,6,8,0);
        List<Integer> oddNumbers=Arrays.asList(1,3,5,7,9);

        List<List<Integer>> master_list=Arrays.asList(naturalNumbers,evenNumbers,oddNumbers);

        System.out.println("Master List is:\t:"+master_list);
        System.out.println(ANSI_RED+"Three main Lists are:");
        master_list.stream().forEach(integers -> System.out.println(integers));

        System.out.println(ANSI_CYAN+"\nAPPROACH#1    :Each elements of lists are:");
        master_list.stream().forEach(integers -> integers.stream().map(integer -> integer+",").forEach(System.out::print));

        System.out.println(ANSI_GREEN+"Each lists sizes are");
        master_list.stream().map(integers -> integers.size()).forEach(System.out::println);



    }
}
