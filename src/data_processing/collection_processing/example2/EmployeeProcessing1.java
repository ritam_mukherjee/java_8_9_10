package data_processing.collection_processing.example2;




import data_processing.map_processing.example2.Employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class EmployeeProcessing1 {
    public static void main(String[] args) {

        Employee employee1=new Employee(11,"Ram");
        Employee employee2=new Employee(22,"Shyam");
        Employee employee3=new Employee(33,"Jadu");
        Employee employee4=new Employee(44,"Madhu");

        List<Employee> employees1=new ArrayList<>(Arrays.asList(employee1,employee2,employee3,employee4));
        System.out.println(ThreadColor.ANSI_BLACK+"Employees are");
        System.out.println(employees1);


        System.out.println(ThreadColor.ANSI_CYAN+"The method sort() takes a comparator to sort the element");
        employees1.sort(Comparator.comparing(Employee::getEname));
        System.out.println(employees1);

        System.out.println(ThreadColor.ANSI_YELLOW+"The method replaceAll()  based on condition change it's elements");
        employees1.replaceAll(employee -> new Employee(employee.getEmpId(),employee.getEname().toUpperCase()));
        System.out.println(employees1);


        System.out.println(ThreadColor.ANSI_RED+"The method removeIf()  based on condition change it's elements");
        employees1.removeIf(employee -> employee.getEmpId()<20);
        System.out.println(employees1);

        Employee employee5 = new Employee(55, "Gopal");
        Employee employee6 = new Employee(66, "Rakhal");
        Employee employee7 = new Employee(77, "Akash");
        Employee employee8 = new Employee(88, "Pallab");

        List<Employee> employees2=new ArrayList<>(Arrays.asList(employee3,employee4,employee5,employee6));

        employees1.stream().forEach(employee -> employees2.add(employee));
        System.out.println(employees2);

    }
}
