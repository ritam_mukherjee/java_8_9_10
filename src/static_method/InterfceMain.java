package static_method;

/**
 * java 1.8 onwards we can declare main method inside interface and run that and get desired output
 */
public interface InterfceMain {
    static void main(String[] args) {
        System.out.println("Main method from interface");
    }
}
