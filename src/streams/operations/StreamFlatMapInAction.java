package streams.operations;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamFlatMapInAction {
    public static void main(String[] args) {
        try(
                Stream<String> geetaLines = Files.lines(Paths.get("src\\streams\\operations\\BhagwadGeeta.txt"));
                Stream<String> gaytriLines = Files.lines(Paths.get("src\\streams\\operations\\Gayatri.txt"));
                Stream<String> krishnaLines = Files.lines(Paths.get("src\\streams\\operations\\KrishnaChant.txt"));
        ){

            String concatenatedString = Stream.of(geetaLines, gaytriLines, krishnaLines)
                    .flatMap(Function.identity())
                    .flatMap(line -> Pattern.compile(" ").splitAsStream(line))
                    .collect(Collectors.joining(","));

            System.out.println(concatenatedString);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
