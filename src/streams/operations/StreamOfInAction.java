package streams.operations;

import java.io.Serializable;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamOfInAction {
    public static void main(String[] args) {
        String collectRawStream =
                Stream.of(
                        Stream.of(1, 2, 3, 4),
                        Stream.of(1.1, 2.2, 3.3, 4.4),
                        Stream.of("Ritam", "Poulami", "Astha", "Teena")
                )
                        .map(String::valueOf)
                        .collect(Collectors.joining(","));

        System.out.println(Colors.ANSI_BLUE+collectRawStream);

        String collectFineTuned =
                Stream.of(
                        Stream.of(1, 2, 3, 4),
                        Stream.of(1.1, 2.2, 3.3, 4.4),
                        Stream.of("Ritam", "Poulami", "Astha", "Teena")
                )
                        .flatMap(stream -> stream)
                        .map(String::valueOf)
                        .collect(Collectors.joining(","));
        System.out.println(Colors.ANSI_RED+collectFineTuned);


        String collectThroughIdentity = Stream.of(
                Stream.of(1, 2, 3, 4),
                Stream.of(1.1, 2.2, 3.3, 4.4),
                Stream.of("Ritam", "Poulami", "Astha", "Teena")
        )
                .flatMap(Function.identity())
                .map(String::valueOf)
                .collect(Collectors.joining(","));

        System.out.println(Colors.ANSI_GREEN+collectThroughIdentity);


        String collectThroughFlatmapIdenity = Stream.of(
                Stream.of(1, 2, 3, 4),
                Stream.of(1.1, 2.2, 3.3, 4.4),
                Stream.of("Ritam", "Poulami", "Astha", "Teena")
        )
                .flatMap(Function.identity())
                .reduce(Colors.ANSI_CYAN + "", (s, serializable) -> s + ","+ serializable , String::concat);
        System.out.println(collectThroughFlatmapIdenity);

    }
}
