package streams.operations;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamIterationInAction {
    public static void main(String[] args) {


        Stream.iterate("", s -> s.length()<=5, s -> s+"+").forEach(System.out::println);

        IntStream.iterate(0,value -> value<=10,operand -> operand+1).forEach(System.out::println);


        DoubleStream.iterate(0,value -> value<=5,operand -> operand+1).forEach(System.out::println);

        int arr[]={11,22,33,44,55};
        IntStream.iterate(0,value -> value<arr.length,operand -> operand+1)
                .map(operand -> arr[operand])
                .forEach(System.out::println);


        IntStream.iterate(0,operand -> operand+1).limit(5)
                .map(operand -> arr[operand])
                .forEach(System.out::println);
    }
}
