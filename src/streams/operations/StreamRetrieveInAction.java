package streams.operations;

import streams.Colors;

import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamRetrieveInAction {
    public static void main(String[] args) {

        String chant="hare rama hare rama krishna krishna hare hare, hare krishna hare krishna rama rama hare hare";

        IntStream chantChars = chant.chars();
        System.out.println(Colors.ANSI_GREEN+"chant having characters:"+chantChars.count());
        Stream<String> hareSplitter = Pattern.compile("hare").splitAsStream(chant);
        System.out.println(hareSplitter.collect(Collectors.joining(",")).toString());


    }
}
