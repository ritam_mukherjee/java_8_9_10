package streams.operations;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamStateInAction {
    public static void main(String[] args) {
        try(
                Stream<String> geetaLines = Files.lines(Paths.get("src\\streams\\operations\\BhagwadGeeta.txt"));
                Stream<String> gaytriLines = Files.lines(Paths.get("src\\streams\\operations\\Gayatri.txt"));
                Stream<String> krishnaLines = Files.lines(Paths.get("src\\streams\\operations\\KrishnaChant.txt"));
        ){


            String sortedDistinctString = Stream.of(geetaLines, gaytriLines, krishnaLines)
                    .flatMap(Function.identity())
                    .flatMap(line -> Pattern.compile(" ").splitAsStream(line))
                    .distinct()
                    .sorted()
                    .collect(Collectors.joining(","));

            System.out.println(sortedDistinctString);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
