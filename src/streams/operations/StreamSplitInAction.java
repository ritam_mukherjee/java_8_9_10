package streams.operations;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * lines();
 * splitAsStream()
 *  * If we provide lines method over Files class we will get stream of String
 */
public class StreamSplitInAction {
    public static void main(String[] args) {
        try(
                Stream<String> geetaLines = Files.lines(Paths.get("src\\streams\\operations\\BhagwadGeeta.txt"));
                Stream<String> gaytriLines = Files.lines(Paths.get("src\\streams\\operations\\Gayatri.txt"));
                Stream<String> krishnaLines = Files.lines(Paths.get("src\\streams\\operations\\KrishnaChant.txt"));
        ){

           geetaLines.map(s -> s.split(" ")).forEach(strings -> System.out.println(Arrays.toString(strings)));

            String elements = gaytriLines
                    .map(line -> Pattern.compile(" ").splitAsStream(line))
                    .flatMap(Stream::distinct)
                    .map(String::valueOf)
                    .collect(Collectors.joining(","));
            System.out.println(elements);


            String colectKrishnaChantElements = krishnaLines
                    .flatMap(line -> Pattern.compile(" ").splitAsStream(line))
                    .collect(Collectors.joining(","));
            System.out.println(colectKrishnaChantElements);

           /* //also can write this way
            String elements2 = gaytriLines
                    .map(line -> Pattern.compile(" ").splitAsStream(line))
                    .map(String::valueOf)
                    .collect(Collectors.joining(","));
            System.out.println(elements);*/


            // geetaLines.map(s -> s.split(" ")).map(strings -> )




        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
