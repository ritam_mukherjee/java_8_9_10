package streams.operations;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamConcatenationInAction {

    public static void main(String[] args) {

        /*stream concat and reduction through reduce()*/
        System.out.println(Stream.concat(
                Stream.of(1,2,3,4),
                Stream.of("Ritam","poulami","Astha","Teena"))
                .reduce((s, serializable) -> s+","+serializable).get());

        /*stream concat and reduction through Collectors collect*/
        System.out.println(Stream.concat(
                Stream.of(1,2,3,4),
                Stream.of("Ritam","poulami","Astha","Teena"))
                .map(String::valueOf)
                .collect(Collectors.joining(",")));


        try(
            Stream<String> geetaLines = Files.lines(Paths.get("src/streams/operations/BhagwadGeeta.txt"));
            Stream<String> gaytriLines = Files.lines(Paths.get("src/streams/operations/Gayatri.txt"));
        ){

            Stream.concat(gaytriLines,geetaLines).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
