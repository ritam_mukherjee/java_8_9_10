package streams;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayStreamInAction {
    public static void main(String[] args) {
        int[] array={1,3,3,0,0,0};
        Arrays.stream(array).forEach(value -> System.out.println(value));
    }
}
