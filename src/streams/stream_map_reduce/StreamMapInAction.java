package streams.stream_map_reduce;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamMapInAction {
    public static void main(String[] args) {
        Stream.Builder<Object> personStream = Stream.builder()
                .add(new Person("Ritam", 22, true, "Kolkata"))
                .add(new Person("Poulami", 20, true, "London"))
                .add(new Person("Astha", 20, false, "New York"))
                .add(new Person("Tina", 21, false, "Madrid"));


        personStream.build()
                .map(o -> (Person)o)
                .map(person -> person.getAge())
                .filter(r -> r<21).forEach(System.out::println);

    }
}
