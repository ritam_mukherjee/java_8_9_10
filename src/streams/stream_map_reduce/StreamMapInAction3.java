package streams.stream_map_reduce;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * usge of intermediate peek() operation
 */
public class StreamMapInAction3 {
    public static void main(String[] args) {
        Stream.Builder<Object> personStream = Stream.builder()
                .add(new Person("Ritam", 22, true, "Kolkata"))
                .add(new Person("Poulami", 20, true, "London"))
                .add(new Person("Astha", 20, false, "New York"))
                .add(new Person("Tina", 21, false, "Madrid"));



        Stream.Builder<Object> personStream1=personStream;
        String personList = personStream1.build()
                .map(o -> (Person) o)
                .peek(person -> System.out.println("Person Details:"+person))
                .filter(person -> person.getAge() < 21)
                .map(person -> person.getName())
                .collect(Collectors.joining(","));
        System.out.println(personList);

    }
}
