package streams;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamOfInAction {
    public static void main(String[] args) {

        Stream.of(8,5,3,9,4,1,0,7,6,2)
                .sorted()
                .map(integer -> String.valueOf(integer))
                .forEach(s -> System.out.print(s+","));

        System.out.println("\n------------------------------");

        String[] days={"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        String dayStream = Stream.of(days).collect(Collectors.joining(","));
        System.out.println(dayStream);
    }
}
