package streams.properties;

import java.util.stream.Stream;

public class SingleProcessing {
    public static void main(String[] args) {

        /*this piece of code will throw exception*/
        Stream<Integer> streams1=Stream.of(0,1,2,3,4,5);
        streams1.limit(2);
       /* streams1.limit(2).forEach(System.out::println);*/

        /*this piece of code will work as expected*/
        Stream<Integer> streams2=Stream.of(0,1,2,3,4,5);
        streams2.limit(2).forEach(System.out::println);

       /* fix problem 1*/
        Stream<Integer> streams3=Stream.of(0,1,2,3,4,5);
        Stream<Integer> streamIntermediate = streams3.limit(2);
        streamIntermediate.limit(2).forEach(System.out::println);
    }
}
