package streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamOperationInAction {
    public static void main(String[] args) {
        List<String> days = Arrays
                .asList("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

        /*forEach(): use for processing elements of Stream.
        stream by default we can traverse all elements by using forEach() method*/
        days.stream().forEach(s -> System.out.print(s+"<->"));
        days.stream().forEach(System.out::println);





        /*count(): by this method we cn get count of elements*/
        System.out.println(Colors.ANSI_GREEN+"Days count:   " + days.stream().count());

        /**map() operation:
         * By using map method we do some special operation over each element of stream
            Here no reduction happen, count will be as it is*/
        String daysInLowerCase = days.stream()
                .map(s -> s.toLowerCase())
                .collect(Collectors.joining(","));
        System.out.println(Colors.ANSI_RED+"Days in lower case :"+daysInLowerCase);

        /**filter() operation:
         * By using reduce method we do some reduction over elements of stream based on condition.
         Here reduction happen, count will be less only*/

        String shorterDays = days.stream()
                .filter(s -> s.length() <= 6)
                .collect(Collectors.joining(","));
        System.out.println(Colors.ANSI_CYAN+"Shorter days are:   "+shorterDays);


        /*Elements are sorted based on default sorting order*/
        String daysSortedOrder = days.stream()
                .sorted()
                .collect(Collectors.joining(","));
        System.out.println(Colors.ANSI_YELLOW+"APPROACH 1:Sorted days are:   "+daysSortedOrder);


        /*Elements are sorted based on default sorting order*/
        String daysSortedOrder2 = days.stream()
                .sorted((i1,i2)-> i1.compareTo(i2))
                .collect(Collectors.joining(","));
        System.out.println(Colors.ANSI_YELLOW+"APPROACH 2: Sorted days are:   "+daysSortedOrder2);

        /*Elements are sorted based on customized sorting order*/
        String daysReverseSortedOrder = days.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.joining(","));
        System.out.println(Colors.ANSI_YELLOW+"APPROACH 1:Reverse Sorted days are:   "+daysReverseSortedOrder);

        /*Elements are sorted based on customized sorting order*/
        String daysReverseSortedOrder2 = days.stream()
                .sorted((i1,i2)-> i2.compareTo(i1))
                .collect(Collectors.joining(","));
        System.out.println(Colors.ANSI_YELLOW+"APPROACH 2:Reverse Sorted days are:   "+daysReverseSortedOrder2);
        days.stream()
                .min(Comparator.naturalOrder())
                .ifPresent(day ->
                        System.out.println(Colors.ANSI_GREEN+"Minimum day: "+day));
        days.stream()
                .max(Comparator.naturalOrder())
                .ifPresent(day ->
                        System.out.println(Colors.ANSI_GREEN+"Maximum day: "+day));


        System.out.println(Colors.ANSI_BLACK+"Days after converting array are:");
        String[] daysArray = days.stream().toArray(String[]::new);
        System.out.println(Arrays.toString(daysArray));

    }
}
