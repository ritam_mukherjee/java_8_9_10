package streams.boxed;

import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamBoxInAction {
    public static void main(String[] args) {
        // Creating an IntStream
        IntStream intStream = IntStream.range(3, 8);
        DoubleStream doubleStream=DoubleStream.of(1.0,2.0,3.0);

        /*This line will throw compile time excepton*/
        Stream<? extends Number> stream = Stream.concat(intStream.boxed(), doubleStream.boxed());
     //   stream.collect(Collectors.toList()).parallelStream().forEach(System.out::println);


        stream.collect(Collectors.toList()).parallelStream().forEach(number -> {
            if(number instanceof Double)
                System.out.println(number +":is Double");
            else
                System.out.println(number +":is not Double");
        });
    }
}
