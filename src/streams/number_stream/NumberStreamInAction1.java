package streams.number_stream;

import map_reduce.People;
import map_reduce.Person;

import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.function.ToIntFunction;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class NumberStreamInAction1 {
    public static void main(String[] args) {
        List<Person> people = new People().getPeople();

        people.stream()
                .mapToInt(person -> person.getAge())
                .filter(age->age>=22)
                .average();


        IntStream.of(1,2,3,4,5)
                .boxed();


        DoubleStream doubleStream = DoubleStream.of(1, 2, 3, 4, 5);

        Stream<Double> streamDouble = DoubleStream.of(1, 2, 3, 4, 5).mapToObj(value -> value);

        System.out.println( DoubleStream.of(1, 2, 3, 4, 5).max().getAsDouble());
        System.out.println(DoubleStream.of(1, 2, 3, 4, 5).min().getAsDouble());
        System.out.println(DoubleStream.of(1, 2, 3, 4, 5).sum());

        DoubleSummaryStatistics doubleSummaryStatistics = doubleStream.summaryStatistics();
        System.out.println("max:"+doubleSummaryStatistics.getMax()+"\tmin:"+doubleSummaryStatistics.getMin()
                +"\tavg:"+doubleSummaryStatistics.getAverage());

        doubleSummaryStatistics.accept(6);
        doubleSummaryStatistics.accept(7);
        doubleSummaryStatistics.accept(8);
        System.out.println("max:"+doubleSummaryStatistics.getMax()+"\tmin:"+doubleSummaryStatistics.getMin()
                +"\tavg:"+doubleSummaryStatistics.getAverage());

    }
}
