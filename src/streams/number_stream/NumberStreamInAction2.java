package streams.number_stream;

import java.util.function.ToIntFunction;

public class NumberStreamInAction2 {
    public static void main(String[] args) {
        ToIntFunction toIntFunction= value -> (int)value;
        int i = toIntFunction.applyAsInt(1);

        System.out.println(i);
    }
}
