package streams.stream_builder;

import map_reduce.People;
import map_reduce.Person;

import java.util.List;
import java.util.stream.Stream;

public class StreamBuilderInAction2 {
    public static void main(String[] args) {
        List<Person> people = new People().getPeople();
        Stream<Person> personStream=people.stream();

        System.out.println(Colors.ANSI_RED+" constant stream by `generate()` method");
        Stream.generate(() -> 5).limit(5).forEach(System.out::println);


        System.out.println(Colors.ANSI_BLUE+" variable argument stream");
        Stream.of("one","two","three","four","five")
                .limit(5).forEach(System.out::println);

       /* two arguments `iterate()` methods, can create infinite loop if we don't provide limit*/
        System.out.println(Colors.ANSI_GREEN+" Constant stream");
        Stream.iterate("+",s -> s=s+"+").limit(5)
                .forEach(System.out::println);

        System.out.println("---------------------");
        /* three arguments `iterate()` method create finite loop*/
        Stream.iterate("+",s -> s=s+"+").limit(5)
                .forEach(System.out::println);

        System.out.println(Colors.ANSI_CYAN+"constant stream with three arguments");
        Stream.iterate("+",s -> s.length()<5,s -> s=s+"+")
                .forEach(System.out::println);

    }
}
