package streams.stream_builder;

import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * StreamBuilder
 * add()
 * accept()
 */
public class StreamBuilderInAction {
    public static void main(String[] args) {

        Stream.Builder<Object> buildStream = Stream.builder().add("one").add("two");
        buildStream.accept("three");
        buildStream.accept("four");
        buildStream.accept("five");
        buildStream.build().forEach(System.out::println);
    }
}
