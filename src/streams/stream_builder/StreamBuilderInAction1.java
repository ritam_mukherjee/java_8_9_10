package streams.stream_builder;

import streams.Colors;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamBuilderInAction1 {
    public static void main(String[] args) {
        try {
            Stream<String> lines = Files.lines(Paths.get("src\\streams\\stream_builder\\BhagwadGeeta.txt"));
            lines.forEach(System.out::println);

            List<Integer> collect = Files
                    .lines(Paths.get("src\\streams\\stream_builder\\BhagwadGeeta.txt"))
                    .map(s -> s.chars()).
                            map(intStream -> intStream.boxed())
                    .map(integerStream -> integerStream.reduce((integer, integer2) -> integer + integer2).get())
                    .collect(Collectors.toList());

            System.out.println(collect);

            String s2=Files
                    .lines(Paths.get("src\\streams\\stream_builder\\BhagwadGeeta.txt"))
                    .map(s -> s.chars())
                    .flatMap(intStream -> intStream.boxed())
                    .reduce("",(s, integer) -> s+integer+ ",",String::concat);
            System.out.println(s2);



        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
