package streams;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/*
concat()

 */
public class StreamOperationInAction2 {

    public static void main(String[] args) {
        Stream<String> firstStream = Stream.of("ritam", "astha", "poulami");
        Stream<String> secondStream =Stream.of("tina","ramisha","piyasa");

        /*
        concat()
         */
        System.out.println(Stream.concat(firstStream, secondStream).collect(Collectors.joining(",")));


        Stream.of(
                Stream.of("ritam", "astha", "poulami"),
                Stream.of("tina","ramisha","piyasa"))
            .flatMap(
                stringStream -> stringStream.map(s -> s.length()))
                    .collect(Collectors.toList()).
                    forEach(integer -> System.out.println(integer));


    }
}
