package flatmaps;

import java.util.Arrays;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlatMapInAction1 {
    public static void main(String[] args) {
        String[][] data = new String[][]{{"a", "b"}, {"c", "d"}, {"e", "f"},{"g","a"}};

        //Stream<String[]>
        Stream<String[]> temp = Arrays.stream(data);

        //filter a stream of string[], and return a string[]?

        System.out.println(
                Arrays.stream(data).filter(x1 -> "a".equals(x1.toString()))
                    .reduce("", (s2, strings1) -> strings1 + "s", String::concat));

        System.out.println(
                Arrays.stream(data)
                        .flatMap(x -> Arrays.stream(x))
                        .filter(x -> "a".equals(x.toString()))
                        .reduce("", (s, strings) -> strings + s, String::concat));
    }
}
