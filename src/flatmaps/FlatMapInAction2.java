package flatmaps;

import java.util.Arrays;
import java.util.List;

public class FlatMapInAction2 {
    public static void main(String[] args) {


        List<List<String>> list = Arrays.asList(
                Arrays.asList("a","b","c"),
                Arrays.asList("A","B","C"),
                Arrays.asList("1","2","3"));

        list.stream().forEach(strings -> System.out.println(strings));

        list.stream()
                .map(strings -> strings.stream())
                .forEach(stringStream -> stringStream.forEach(System.out::println));


        list.stream()
                .map(strings -> strings.stream())
                .forEach(stringStream -> stringStream.forEach(System.out::println));

        list.stream()
                .flatMap(strings -> strings.stream())
                .forEach(System.out::println);
    }
}
