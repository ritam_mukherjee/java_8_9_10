package flatmaps;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FatMapInAction3 {
    public static void main(String[] args) {


        Stream<String> s1 = Stream.of("a", "b", "c");
        Stream<String> s2 = Stream.of("d", "e", "f");
        Stream<String> s3 = Stream.of("g", "h", "i");


        String flattenString =
                Stream.of(s1, s2, s3).flatMap(Function.identity()).collect(Collectors.joining(","));
        System.out.println(flattenString);
    }
}

