package map_reduce;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;

/**
 * Map Reduce Not Associative Operation
 */
public class MapReductionNonAssociative {

    public static int reduce(
            List<Integer> values,
            int valueIfEmpty,
            BinaryOperator<Integer> reduction) {

        int result = valueIfEmpty;
        for (int value : values) {
            result = reduction.apply(result, value);
        }
        return result;
    }

    public static void main(String[] args) {

        List<Integer> ints = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8,9);

        List<Integer> ints1 = Arrays.asList(0, 1, 2, 3, 4);
        List<Integer> ints2 = Arrays.asList(5, 6, 7, 8, 9);

        BinaryOperator<Integer> average = (i1, i2) -> (i1+i2)/2 ;

        int reduction1 = reduce(ints1, 0, average);
        int reduction2 = reduce(ints2, 0, average);
        int reduction = reduce(Arrays.asList(reduction1, reduction2), 0, average);
         //int reduction = reduce(ints, 0, average);

        System.out.println("Reduction : " + reduction);

    }
}
