package map_reduce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MapReduceInAction {
    public static void main(String[] args) {
        List<Person> people = new People().getPeople();

        /*reduce with Accumulator*/

        int ageAvg = people.stream()
                .mapToInt(person -> person.getAge()) /*map process*/
                .filter(age -> age > 21)  /*filter step*/
                .reduce((age1, age2) -> (age1 + age2) / 2).getAsInt();/*reduction step*/

        System.out.println("ageAvg:"+ageAvg);

        /*reduce with Accumulator and Identifier*/
        Integer ageMax = people.stream()
                .reduce(0,
                        (integer, person) -> integer + person.getAge(), Integer::max);

        System.out.println("ageMax:"+ageMax);

        /*reduce with Accumulator ,Identifier & Combiner */
        Integer ageSum = people.stream()
                .reduce(0,
                        (sum, person) -> {
                            System.out.format("accumulator: sum=%s; person=%s\n", sum, person);
                            return sum += person.getAge();
                        },
                        (sum1, sum2) -> {
                            System.out.format("combiner: sum1=%s; sum2=%s\n", sum1, sum2);
                            return sum1 + sum2;
                        });

        System.out.println("ageSum:"+ageSum);


        ArrayList<Integer> agesList = people.stream()
                .reduce(
                        new ArrayList<Integer>(),
                        (list, person) -> {
                            list.add(person.getAge());
                            return list;

                        },
                        (list1, list2) -> {
                            list1.addAll(list2);
                            return list1;
                        }
                );
        System.out.println(agesList);


        /* parallel Stream*/
        ageSum=people.parallelStream()
                .reduce(0,
                        (partialAgeResult, person) -> partialAgeResult + person.getAge(), Integer::sum);
        System.out.println("ageSum:"+ageSum);

    }
}
