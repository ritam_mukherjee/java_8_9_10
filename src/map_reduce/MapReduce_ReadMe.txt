Map- Reduce algorithm having basically 3 steps:

Scenario: we are getting List of persons and from that going to get average age of people who are older than 20.

1. Mapping:
    Gets List of persons and returns List of age. and both case size of the map should be same
            List<Person> -> List<Integer>  [both case size should be same ]
     Also the order of Age should be same as order of Employee. Heremaintaing order also an important aspect.

2. Filtering:
     The List of Ages and returns List of ages having List of those ages where Age is more than 20.
      Filtering done by predicate that is Age>20
            List<Integer>  ->  List<Integer> with reduce count

3. Reducing:
      Third step is reduction step which perform mathematical average. It's equivalent to SQL 'Aggregation'


Filter:
1. Filter always works on stream of data
2. It always takes an predicate as it's parameter and Based on predicate filter out source of data

predicate is a functional interface having a single method call test which returns boolean value
[ it also has default method like 'and', 'or', 'negate']

we can chain numbers of predecate, but calling of these predecate not depends on priority rather than
 order they have called