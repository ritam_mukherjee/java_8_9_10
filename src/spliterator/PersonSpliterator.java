package spliterator;

import java.util.Spliterator;
import java.util.function.Consumer;

/**
 *
 * @author Ritam
 */
public class PersonSpliterator implements Spliterator<Person> {

    /*based on lineSpliterator object this custom spliterator works, hence `lineSpliterator` we have to pass as variable*/
    private final Spliterator<String> lineSpliterator;
    private String name;
    private int age;
    private String city;

    public PersonSpliterator(Spliterator<String> lineSpliterator) {
        this.lineSpliterator = lineSpliterator ;
    }

    /*
    this tryAdvance method is equivalent to next
    this method returns as long elements are three and false when done
     */
    @Override
    public boolean tryAdvance(Consumer<? super Person> action) {
        if (this.lineSpliterator.tryAdvance(line -> this.name = line) &&
            this.lineSpliterator.tryAdvance(line -> this.age = Integer.parseInt(line)) &&
            this.lineSpliterator.tryAdvance(line -> this.city = line)) {
            
            Person p = new Person(name, age, city);
            action.accept(p);
            return true;
        } else {
            return false;
        }
    }

    /*we are not implementing this method because we don't want parallel execution*/
    @Override
    public Spliterator<Person> trySplit() {
        return null;
    }

    /*
     * divides lines by 3 to make custom spliterator will span 3 lines
     * other words each three lines are scope of this spliterator
     */
    @Override
    public long estimateSize() {
        return lineSpliterator.estimateSize() / 3;
    }

    /*
     * we are not going to change characteristics of this custom spliterator
     * just specifying characteristics of this spliterator is same as lineSpliterator
     */
    @Override
    public int characteristics() {
        return lineSpliterator.characteristics();
    }
}
