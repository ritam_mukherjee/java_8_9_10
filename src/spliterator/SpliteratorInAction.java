package spliterator;

import java.util.ArrayList;
import java.util.Spliterator;

public class SpliteratorInAction {
    public static void main(String[] args) {
        ArrayList<Integer> al = new ArrayList<>();

        // Add values to the array list.
        al.add(1);
        al.add(2);
        al.add(-3);
        al.add(-4);
        al.add(5);

        System.out.println("Print elements which are positive");
        al.spliterator().forEachRemaining(integer -> {
            if(integer>0)
                System.out.println(integer);
        });

        System.out.println("Print square of that");
        al.spliterator().forEachRemaining(integer -> System.out.println(integer*integer));

        System.out.println("---------------------trySplit()---------------------------");

        System.out.println("spliterator split into two parts");
        Spliterator spliterator1=al.spliterator();
        Spliterator spliterator2=spliterator1.trySplit();
        System.out.println("second Iterator's elements: spliterator2:");
        spliterator2.forEachRemaining(integer -> System.out.println(integer));

        System.out.println("first Iterator's elements: spliterator2:");
        spliterator1.forEachRemaining(integer -> System.out.println(integer));

        System.out.println("--------------------tryAdvance()---------------------------");
        Spliterator trySpliterator=al.spliterator();
        while(trySpliterator.tryAdvance(integer -> System.out.println("No:"+integer)));

        Spliterator trySpliterator2=al.spliterator();
        while(trySpliterator2.tryAdvance(integer -> System.out.println(integer+",")));


        Spliterator spliterator3=al.spliterator();
        System.out.println("Size is"+spliterator3.getExactSizeIfKnown());
        spliterator3.forEachRemaining(o -> System.out.println(o));
        System.out.println("Size is"+spliterator3.getExactSizeIfKnown());

        Spliterator spliterator4=al.spliterator();
        System.out.println("Size is"+spliterator4.getExactSizeIfKnown());
        /*spliterator4.forEachRemaining(integer -> {
            if(integer>0)
                System.out.println(integer);
        });*/
        System.out.println("Size is"+spliterator4.getExactSizeIfKnown());
    }
}
