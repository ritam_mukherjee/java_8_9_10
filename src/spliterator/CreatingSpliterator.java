package spliterator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;



/**
 *
 * @author Ritam
 */
public class CreatingSpliterator {

    public static void main(String[] args) {
        
        Path path = Paths.get("src/spliterator/people.txt");
        
        try (Stream<String> lines = Files.lines(path);) {

            /*this does line by line spliterator*/
            Spliterator<String> lineSpliterator = lines.spliterator();
            Spliterator<Person> peopleSpliterator = new PersonSpliterator(lineSpliterator);
            
            Stream<Person> people = StreamSupport.stream(peopleSpliterator, false);
            people.forEach(System.out::println);
            
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
