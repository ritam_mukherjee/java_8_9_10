package optionals;

import map_reduce.Person;

import java.util.List;
import java.util.OptionalDouble;

public class OptionalsInAction {
    public static void main(String[] args) {
        List<Person> people=new People().getPeople();

        OptionalDouble average = people.stream().mapToInt(person -> person.getAge()).average();
    }
}
