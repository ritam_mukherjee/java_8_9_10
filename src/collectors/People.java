package collectors;

import java.util.Arrays;
import java.util.List;

public class People {

    List<Person> people;

    public People() {
        people = Arrays.asList(
                new Person("Ritam", 22, true, "Kolkata"),
                new Person("Poulami", 20, true, "London"),
                new Person("Astha", 20, false, "New York"),
                new Person("Tina", 21, false, "Madrid")

        );
    }

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }
}
