package collectors;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 *
 */
public class CollectorProgram4 {

    public static void main(String[] args) {

        List<Person> peopleList = new People().getPeople();
        Map<Boolean, List<Person>> partitionedPeople =
                peopleList.stream()
                        .collect(Collectors.partitioningBy(person -> person.getAge() < 21));

        System.out.println(Colors.ANSI_PURPLE + "The map after partitioning by:\n\t" + partitionedPeople);


        /*grouping by age criteria*/
        Map<Boolean, List<Person>> groupedPeopleAdultCriteria = peopleList.stream()
                .collect(Collectors.groupingBy(person -> person.getAge() < 21));

        System.out.println(Colors.ANSI_CYAN + "The map after grouping by:\n\t" + groupedPeopleAdultCriteria);

        /*grouping by age*/
        Map<Integer, List<Person>> groupedPeopleByAge = peopleList.stream()
                .collect(Collectors.groupingBy(person -> person.getAge()));

        System.out.println(Colors.ANSI_YELLOW + "The map after grouping by:\n\t" + groupedPeopleByAge);

        Map<Integer, Long> groupedPeopleCountByAge = peopleList.stream()
                .collect(Collectors.groupingBy(person -> person.getAge(),
                        Collectors.counting()));

        System.out.println(Colors.ANSI_RED + "The map after grouping by:\n\t" + groupedPeopleCountByAge);

        Map<Integer, List<String>> GroupedPeopleListByAge = peopleList.stream()
                .collect(Collectors.groupingBy(person -> person.getAge(),
                        Collectors.mapping(person -> person.getName(), Collectors.toList()))
                );

        System.out.println(Colors.ANSI_BLUE + "The map after grouping by:\n\t" + GroupedPeopleListByAge);

        /*Named of the people will be recorded in a map and by default ill be sorted and removed duplicate*/
        Map<Integer, TreeSet<String>> groupedPeopleMapByAge = peopleList.stream()
                .collect(
                        Collectors.groupingBy(person -> person.getAge(),
                                Collectors.mapping(
                                        person -> person.getName(),
                                        Collectors.toCollection(TreeSet::new)
                                )
                        )
                );
        System.out.println(Colors.ANSI_GREEN + "The map after grouping by:\n\t" + groupedPeopleMapByAge);


        Map<Integer, List<Person>> collectUnmodifiableMap = peopleList.stream()
                .collect(
                        Collectors.collectingAndThen(
                                Collectors.groupingBy(person -> person.getAge()),
                                Collections::unmodifiableMap
                        )
                );

        System.out.println(Colors.ANSI_WHITE
                + "The map after grouping by:\n\t" + collectUnmodifiableMap);







    }
}
