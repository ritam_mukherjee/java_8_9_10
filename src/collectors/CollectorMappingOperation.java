package collectors;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class CollectorMappingOperation {
    public static void main(String[] args) {
        List<Person> peopleList = new People().getPeople();


        /**
         * partitioningBy()
         */
        System.out.println(Colors.ANSI_GREEN + "Get a map of Person based on there Marital status::");
        Map<Boolean, List<Person>> personByMaritalStatus =
                peopleList.stream()
                        .collect(Collectors
                                .partitioningBy(person -> person.getMarried()));
        personByMaritalStatus.entrySet().forEach(System.out::println);
        System.out.println(Colors.ANSI_WHITE+"-----------------------------------------------------");

        System.out.println(Colors.ANSI_RED + "Get a map of Person grouped by Age::");
        Map<Integer, List<Person>> personGroupByAge = peopleList.stream()
                .collect(Collectors.groupingBy(person -> person.getAge()));

        personGroupByAge.entrySet().forEach(System.out::println);
        System.out.println(Colors.ANSI_WHITE+"-----------------------------------------------------");

        System.out.println(Colors.ANSI_CYAN + "Get a map of Person grouped by Age::");

       /* peopleList.stream().
                collect(Collectors.groupingBy(person -> person.getName()),
                        Collectors.toCollection(TreeSet::new)
                );*/

        System.out.println(Colors.ANSI_WHITE+"-----------------------------------------------------");




        /**
         * Getting a map group by age and count of that age
         */
        System.out.println(Colors.ANSI_BLUE + "Get a map of Person grouped by Age::"+
                peopleList.stream()
                .collect(
                        Collectors.groupingBy(person -> person.getAge(),
                                Collectors.counting())
                )
                .entrySet()
                .toString());



        System.out.println(Colors.ANSI_CYAN + "Get a map of Person grouped by Age::");
        Map<Integer, List<Person>> personGroupByName =
                peopleList.stream()
                        .collect(Collectors.groupingBy(person -> person.getAge()));

        System.out.println(Colors.ANSI_WHITE+"-----------------------------------------------------");
    }
}
