package collectors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectorProgram1 {

    public static void main(String[] args) {

        List<Person> peopleList=new People().getPeople();

        System.out.println(Colors.ANSI_RED+"People list_________________");
        peopleList.forEach(System.out::println);

        /**
         * Non concurrent approach:
         * 'marriedPeopleList' is an external ArrayList object which not support parellalism
         */
        List<Person> marriedPeopleList=new ArrayList<>();
        peopleList
                .stream().filter(person -> person.getMarried())
                .forEach(marriedPeopleList::add);

        System.out.println(Colors.ANSI_GREEN+"Married People list[Non Concurrence Approach]___________");
        marriedPeopleList.forEach(System.out::println);

        /**
         * Concurrent Approach
         * Married people list populated directly
         */
        System.out.println(Colors.ANSI_PURPLE+"Married People list[Concurrence Approach]___________");
        peopleList.stream()
                        .filter(person -> person.getMarried())
                        .collect(Collectors.toList())
                        .forEach(System.out::print);
    }
}
