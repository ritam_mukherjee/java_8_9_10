package collectors;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * maxBy()
 */
public class CollectorProgram2 {
    public static void main(String[] args) {

        List<Person> peopleList=new People().getPeople();
        /**
         * We can compute aggregation operation over list based on a particular parameter
         * it returns an optional object if at all the condition statisfy.
         */
        System.out.println(Colors.ANSI_RED+"The oldest person of the list is :");
        Optional<Person> oldest=peopleList
                .stream()
                .collect(Collectors
                        .maxBy((age1, age2) -> age1.getAge()));

        oldest.ifPresent(System.out::println);

        /**
         * Aggregate operation with a definite value
         */
        double ageAverage=peopleList
                .stream()
                .collect(
                        Collectors.averagingDouble(people->people.getAge())
                );

        System.out.println(Colors.ANSI_PURPLE+"The Age Average is:"+ageAverage);

    }
}
