package collectors;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Collectors.joining*(
 */
public class CollectorProgram3 {
    public static void main(String[] args) {
        List<Person> peopleList = new People().getPeople();

        /**
         * Collector also can work after "Reduction'
         */
        String name = peopleList.stream()
                .map(person -> person.getName())  /*reduction step*/
                .collect(Collectors.joining(","));
        System.out.println(Colors.ANSI_PURPLE + "Name List :" + name);
        System.out.println(Colors.ANSI_WHITE+"-----------------------------------------------------");

        /**
         * toSet()
         */
        System.out.println(Colors.ANSI_BLUE + "Age Set :" +
                peopleList.stream()
                        .map(person -> person.getAge())
                        .collect(Collectors.toSet())
                        .toString());
        System.out.println(Colors.ANSI_WHITE+"-----------------------------------------------------");

        /**
         * Collectors(toCollection(<Supplier>)
         * Return type of the collect method imposed to the return type of supplier in natural order
         */
        String naturalOrder=
                peopleList.stream()
                        .map(person -> person.getName())
                        .collect(Collectors.toCollection(TreeSet::new))
                        .stream()
                        .collect(Collectors.joining(","));

        System.out.println(Colors.ANSI_BLACK + "Get a tree set with natural order of person name :" + naturalOrder);
        System.out.println(Colors.ANSI_WHITE+"-----------------------------------------------------");



        /**
         *
         * Collectors(toCollection(<Supplier>)
         * Return type of the collect method imposed to the return type of supplier in reverse order
         */
        String reverseName =
                peopleList.stream()
                        .map(person -> person.getName())
                        .collect(Collectors.toCollection(() -> new TreeSet<>((o1, o2) -> o2.compareTo(o1))))
                        .stream()
                        .collect(Collectors.joining(","));

        System.out.println(Colors.ANSI_BLACK + "Get a tree set with reverse order of person name :" + reverseName);
        System.out.println(Colors.ANSI_WHITE+"-----------------------------------------------------");


    }
}
