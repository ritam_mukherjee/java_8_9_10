package recursion;

import java.util.function.UnaryOperator;

public class RecursionFibonacci {

    UnaryOperator<Integer> fibonacci = x -> x <=2
            ? 1
            : this.fibonacci.apply(x - 1 )+ this.fibonacci.apply(x - 2 );

    public static void main(String[] args) {
        System.out.println(new RecursionFibonacci().fibonacci.apply(7));
    }


}
