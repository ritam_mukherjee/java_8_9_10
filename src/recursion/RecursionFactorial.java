package recursion;

import java.util.function.UnaryOperator;

public class RecursionFactorial {

    UnaryOperator<Integer> factorial = x -> x == 0
            ? 1
            : x * this.factorial.apply(x - 1 );

    public static void main(String[] args) {
        RecursionFactorial recursionFactorial=new RecursionFactorial();

        System.out.println(recursionFactorial.factorial.apply(5));
    }
}
