package daten_n_time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class DateNTime1 {
    public static void main(String[] args) {
        LocalDate today= LocalDate.now();
        System.out.println(today);
        System.out.println(today.getDayOfMonth());
        System.out.println(today.getMonthValue());
        System.out.println(today.getYear());

        System.out.printf("%d-%d-%d\n",today.getDayOfMonth(),today.getMonthValue(),today.getYear());

        LocalTime time= LocalTime.now();
        System.out.println(time);
        System.out.printf("%d::%d::%d::%d\n",time.getHour(),time.getMinute(),time.getSecond(),time.getNano());

        LocalDateTime dateTime=LocalDateTime.now();
        System.out.println(dateTime);
        System.out.printf("%d-%d-%dT%d:%d:%d:%d\n"
                ,dateTime.getDayOfMonth(),dateTime.getMonthValue(),dateTime.getYear()
        ,dateTime.getHour(),dateTime.getMinute(),dateTime.getSecond(),dateTime.getNano());
    }


}
