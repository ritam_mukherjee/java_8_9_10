package daten_n_time;

import java.time.*;
import java.util.Date;

public class DateNTime2 {
    public static void main(String[] args) {
        LocalDateTime birthDate=LocalDateTime.of(1987, Month.MAY,26,8,42,36);
        System.out.println("birth date"+birthDate);
        System.out.println("rice ceremony"+birthDate.plusMonths(6));

        Period period=Period.between(birthDate.toLocalDate(),LocalDate.now());
        System.out.println("Period default format:"+period);
        System.out.println("Period customized format:"
                +period.getYears()+"-"+period.getMonths()+"-"+period.getDays());

        Duration duration=Duration.between(birthDate,LocalDateTime.now());
        System.out.println("Duration default format:"+duration);


        Year year=Year.of(2020);
        System.out.println(year.isLeap()?"Leap year":"Not leap year");
        System.out.println(year.plusYears(2).isLeap()?"Leap year":"Not leap year");


        ZoneId zone=ZoneId.systemDefault();
        System.out.println(zone);
        zone=ZoneId.of("America/Los_Angeles");
        ZonedDateTime zoneTme=ZonedDateTime.now(zone);
        System.out.println(zoneTme);
    }
}
