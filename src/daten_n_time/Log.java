package daten_n_time;

import java.time.LocalTime;

public class Log {
    private LocalTime timeStamp;

    public LocalTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Log(LocalTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "timeStamp=" + timeStamp +
                "";
    }
}
