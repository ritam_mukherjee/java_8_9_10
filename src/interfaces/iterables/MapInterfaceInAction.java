package interfaces.iterables;

import interfaces.City;
import interfaces.People;
import interfaces.ThreadColor;

import javax.xml.crypto.dsig.spec.HMACParameterSpec;
import java.util.*;

public class MapInterfaceInAction {

    public static void main(String[] args) {

        Map<City, List<People>> cityMapper=new HashMap<>();

        City kolkata = new City("Kolkata");
        cityMapper.putIfAbsent(kolkata, new ArrayList(Arrays.asList(
                new People(24,"Tina"),
                new People(23,"Ramisha"))));

        City bangalore = new City("Bangalore");
        cityMapper.putIfAbsent(bangalore, new ArrayList(Arrays.asList(
                new People(25,"Ritam"),
                new People(22,"Astha"),
                new People(22,"Poulami"))));

        System.out.println(ThreadColor.ANSI_GREEN+"List of City and people:");

        cityMapper.forEach((city, people) ->
                System.out.println("city:"+city.getName()+", No of People:"+people.size()));


        /**
         * Contains method not have to use
         * Also get rid of getting null
         */
        System.out.println(cityMapper.getOrDefault(bangalore,Collections.EMPTY_LIST));
        System.out.println(cityMapper.getOrDefault(new City("Mumbai"),Collections.EMPTY_LIST));


        cityMapper.replaceAll((city, people) ->{
            if(city.getName().equals("Bangalore")){
                people.removeIf(people1->people1.getAge()>=24);
            }
            return people;
        });

        System.out.println(cityMapper);


        //compute Things need to add
    }
}
