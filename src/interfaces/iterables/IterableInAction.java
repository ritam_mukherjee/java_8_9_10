package interfaces.iterables;

import interfaces.People;
import interfaces.ThreadColor;

import javax.imageio.plugins.jpeg.JPEGImageReadParam;
import java.util.*;

public class IterableInAction {
    public static void main(String[] args) {

        List<People> peopleList= new ArrayList<>(Arrays.asList(
                new People(25,"Ritam"),
                new People(22,"Astha"),
                new People(24,"Tina"),
                new People(23,"Ramisha"),
                new People(22,"Poulami")));

        System.out.println(ThreadColor.ANSI_GREEN+"List of People:");
        peopleList.forEach(System.out::println);

        System.out.println(ThreadColor.ANSI_CYAN+"List of People after Sorting:");
        peopleList.sort(Comparator.comparing(People::getAge).thenComparing(People::getName));
        peopleList.forEach(System.out::println);

        System.out.println(ThreadColor.ANSI_BLUE+"List of People after Greeting:");
        peopleList.replaceAll(people -> new People(people.getAge(),"Hello "+people.getName()));
        peopleList.forEach(System.out::println);


        System.out.println(ThreadColor.ANSI_RED+"List of People under age 24");
        peopleList.removeIf(people -> people.getAge()>=24);
        peopleList.forEach(System.out::println);

        peopleList.removeAll(peopleList);
        System.out.println(peopleList);



    }
}
