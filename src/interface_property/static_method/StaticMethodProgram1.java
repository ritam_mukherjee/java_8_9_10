package interface_property.static_method;


interface Greeting{
    static String greet(String firstName,String lastName){
        return "Ola "+firstName.concat(" ").concat(lastName);
    }
}
class StudentGreeting implements Greeting{

    public void studentGreet(String firstName,String lastName){
        System.out.println( Greeting.greet(firstName,lastName));
        System.out.println("Welcome to student's world");
    }
}
class TeacherGreeting implements Greeting{

    public void teacherGreet(String firstName,String lastName){
        System.out.println( Greeting.greet(firstName,lastName));
        System.out.println("Welcome to teacher's world");
    }
}
public class StaticMethodProgram1 {
    public static void main(String[] args) {
         new TeacherGreeting().teacherGreet("John","Doe");
         new StudentGreeting().studentGreet("Jack","Hayden");
    }
}
