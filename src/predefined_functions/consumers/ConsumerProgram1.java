package predefined_functions.consumers;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerProgram1 {
    public static void main(String[] args) {
        List<String> students = new ArrayList<>();

        Consumer<String> addstudent = student -> students.add(student);
        Consumer<String> students_print = s -> System.out.println(students);

        Consumer chain_student = addstudent.andThen(students_print);

        chain_student.accept("TOM");
        chain_student.accept("DIK");
        chain_student.accept("HARRY");

    }

}
