package predefined_functions.predicates;

import predefined_functions.Colors;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Predicate joining example
 * and()
 * or()
 */
public class PredicateChainingInAction {
    public static void main(String[] args) {

        /*Function test one predicate as argument and test all elements */
        BiFunction<Integer[],Predicate<Integer>,String> checkArray =
                (integers, integerPredicate) ->
                        Arrays.stream(integers).allMatch(integerPredicate)
                                ?"all values matched"
                                :"One or many values mismatched";

        Consumer<String> printer= System.out::println;


        Predicate<Integer> bigger_than_fifty=integer -> integer>50;
        Predicate<Integer> smaller_than_hundred=integer -> integer<100;

        printer.accept(checkArray.apply(new Integer[]{60,70,80},bigger_than_fifty));
        printer.accept(checkArray.apply(new Integer[]{60,70,80},smaller_than_hundred));

        /*negate joining: checking complement of the predicate*/
        System.out.println(Colors.ANSI_RED + "Testing negate() operator where complement of the condition validated:");
        printer.accept(checkArray.apply(new Integer[]{60,70,80},bigger_than_fifty.negate()));
        printer.accept(checkArray.apply(new Integer[]{60,70,80},smaller_than_hundred.negate()));

        /*and joining*/
        System.out.println(Colors.ANSI_CYAN + "Testing and operator where all conditions are validated:");
        printer.accept(checkArray.apply(new Integer[]{60,70,80},bigger_than_fifty.and(smaller_than_hundred)));
        printer.accept(checkArray.apply(new Integer[]{60,70,80},bigger_than_fifty.and(smaller_than_hundred)));

        /*or joining*/
        System.out.println(Colors.ANSI_GREEN + "Testing or operator where at least  one condition should valid:");
        printer.accept(checkArray.apply(new Integer[]{60,70,120},bigger_than_fifty.or(smaller_than_hundred)));
        printer.accept(checkArray.apply(new Integer[]{60,70,80},bigger_than_fifty.or(smaller_than_hundred)));
    }
}
