package predefined_functions.predicates;

import java.util.function.Function;
import java.util.function.Predicate;
class Employee{
    String name;
    String designation;

    public Employee(String name, String designation) {
        this.name = name;
        this.designation = designation;
    }
}

/**
 * custom Predicate()
 */
public class PredicateProgram3 {

    public static void main(String[] args) {
        Predicate<String > isCEO=Predicate.isEqual("CEO");
        Function<Employee,Boolean>  isEmployeeCEO=employee -> isCEO.test(employee.designation);
        Employee employee1=new Employee("Ritam","CEO");
        Employee employee2=new Employee("Astha","CTO");
        System.out.println(isEmployeeCEO.apply(employee1));
        System.out.println(isEmployeeCEO.apply(employee2));
    }
}
