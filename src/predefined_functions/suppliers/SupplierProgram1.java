package predefined_functions.suppliers;

import java.util.Date;
import java.util.function.Supplier;

public class SupplierProgram1 {
    public static void main(String[] args) {
        Supplier<Date> date_supplier=() -> new Date();
        System.out.println(date_supplier.get());

        Supplier<Double> get_number=() -> Math.random()*100;
        System.out.println(get_number.get());

    }
}
