package predefined_functions.functions;

import predefined_functions.Colors;

import java.util.function.Function;

/**
 * besic functionality of java.util.Function predefined functional interface
 */
public class FunctionProgram1 {
    static Function<String,String> docker_stop_message=container_id -> "******Docker stopped container********* = " + container_id;
    public static void main(String[] args) {
        Function<String,Integer> space_count=s -> s.length()-s.replaceAll(" ","").length();

        System.out.println(space_count.apply("This is it!!!"));

        Function<Integer,Integer> sum=integer -> integer+integer;
        Function<Integer,Integer>  square=integer -> integer*integer;

      /**
       * f1.andThen(f2): First f1 will be applied and then for the result f2 will be applied
       * f1.compose(f2)===>First f2 will be applied and then for the result f1 will be applied.
       * */
        System.out.println(Colors.ANSI_YELLOW+"chaining example------------");
        System.out.println("example:andThen()->"+sum.andThen(square).apply(8));
        System.out.println("example:compose()->"+sum.compose(square).apply(8));

       /* identity() : Returns a function that always returns its input argument*/
        Function<String,String> identical=Function.identity();
        System.out.println(Colors.ANSI_RED+"identity example:"+identical.apply("Ritam"));


    }
}
