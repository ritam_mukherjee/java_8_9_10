package lambda_basic.functional_interface;

@FunctionalInterface
public interface Accelerator {
    public abstract void  accelerate();
}
