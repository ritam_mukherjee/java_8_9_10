package map_operations;

import map_reduce.People;
import map_reduce.Person;

import java.util.List;

public class MapOperationInAction {
    public static void main(String[] args) {
        List<Person> people = new People().getPeople();

        System.out.println("Is all person's age more than 22?-"+
                people.stream().allMatch(person -> person.getAge()>=22));

        System.out.println("Is any person's age more than 22?-"+
                people.stream().anyMatch(person -> person.getAge()>=22));

        System.out.println("Is no person's age more than 22?-"+
                people.stream().noneMatch(person -> person.getAge()>=22));




    }
}
