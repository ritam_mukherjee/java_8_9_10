package map_operations;

import java.util.Optional;
import java.util.OptionalInt;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MapOperations {
    public static void main(String[] args) {

        String str = "Ritam Mukherjee";

        System.out.println(
                str.chars().mapToObj(c -> (char) c).map(String::valueOf).collect(Collectors.joining(",")));


        char[] chars = str.toCharArray();
        System.out.println(
                str.chars().mapToObj(c -> (char) c)
                        .map(character ->
                                IntStream.range(0, str.length()).map(i -> chars[i]).filter(i -> (char) i == character).count())
                        .map(String::valueOf).collect(Collectors.joining(";"))
        );


        /*  print elements*/
        AtomicReference ar = new AtomicReference();
        IntStream.range(0, str.length()).map(i -> chars[i]).distinct().
                forEach(item -> {
                    long count = str.chars().mapToObj(cc -> (char) cc).filter(character -> character.equals((char) item)).count();
                    System.out.println((char) item + "->" + count);
                });


        String charCount = IntStream.range(0, str.length())
                .map(i -> chars[i])
                .distinct().
                        mapToObj(item -> (char) item + "->" +
                                str.chars().mapToObj(cc -> (char) cc).filter(character -> character.equals((char) item)).count()
                        ).collect(Collectors.joining(","));
        System.out.println(charCount);



    /*    IntStream.range(0, str.length()).map(i -> chars[i]).distinct().
                forEach(item -> {
                    Optional<Character> first = str.chars().mapToObj(cc -> (char) cc).filter(character -> character.equals((char) item)).findFirst();
                    System.out.println((char) item + "->" + count);
                });*/

        // System.out.println(asInt);

        Optional<Character> first = str.chars().mapToObj(c -> (char) c).
                distinct().
                filter(character ->
                        str.chars().mapToObj(cc -> (char) cc).anyMatch(character1 -> character1 == character)
                ).findFirst();

        System.out.println(first.get());

        String firstOccurenceIndex = str.chars().mapToObj(c -> (char) c)
                .map(character ->
                        IntStream.range(0, str.length()).map(i -> chars[i]).filter(i -> (char) i == character).findFirst().getAsInt())
                .map(String::valueOf).collect(Collectors.joining(","));

        System.out.println(firstOccurenceIndex);

        //int first = IntStream.of(chars.length).map(i -> chars[i-1]).filter(i -> i == 'a').findFirst().getAsInt();
        //  System.out.println(first);
    }
}
